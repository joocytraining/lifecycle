package com.joocy.lifecycle

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import java.time.ZonedDateTime

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG = "MainActivity"
        val STATE_LAST_SAVED = "lastSaved"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setContentView(R.layout.activity_main)
        lifecycle.addObserver(MainLifecycleObserver())
        if (savedInstanceState != null) {
            Log.d(TAG, "Restoring state onCreate")
            Log.d(TAG, "Restored state is ${savedInstanceState.getString(STATE_LAST_SAVED)}")
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "onRestart")
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d(TAG, "Saving state")
        outState?.putString(STATE_LAST_SAVED, ZonedDateTime.now().toString())
        super.onSaveInstanceState(outState)
    }
}
